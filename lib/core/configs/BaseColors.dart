import 'package:flutter/material.dart';

class BaseColors {
  
  static final Color primary = Colors.blue;
  static final Color success = Colors.green;
  static final Color danger = Colors.red;
  static final Color light = Colors.white;
}