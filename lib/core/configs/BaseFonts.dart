class BaseFonts {

  static final String primaryFontRegular = "NoirPro-Regular";
  static final String primaryFontMedium = "NoirPro-Medium";
  static final String primaryFontSemiBold = "NoirPro-SemiBold";
  static final String primaryFontBold = "NoirPro-Bold";
}