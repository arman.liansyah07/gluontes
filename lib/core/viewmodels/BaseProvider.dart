import 'package:flutter/material.dart';

class BaseProvider extends ChangeNotifier {

  // username
  TextEditingController username = new TextEditingController();
  bool usernameValid = true;
  bool usernameValidMessage = true;
  void setUsernameValid(bool value, {bool isMessage = false}) {
    if(!isMessage) {
      usernameValid = value;
      usernameValidMessage = value;
    }
    else {
      usernameValidMessage = value;
    }
    notifyListeners();
  }

  // password
  TextEditingController password = new TextEditingController();
  bool passwordValid = true;
  bool passwordValidMessage = true;
  void setPasswordValid(bool value, {bool isMessage = false}) {
    if(!isMessage) {
      passwordValid = value;
      passwordValidMessage = value;
    }
    else {
      passwordValidMessage = value;
    }
    notifyListeners();
  }
}