import 'package:flutter/material.dart';
import 'package:gluontes/core/viewmodels/BaseProvider.dart';
import 'package:gluontes/ui/screens/LoginScreen.dart';
import 'package:provider/provider.dart';
import 'core/configs/BaseFonts.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => BaseProvider()),
      ],
      child: MaterialApp(
        home: LoginScreen(),
        theme: ThemeData(
          fontFamily: BaseFonts.primaryFontRegular,
        ),
        debugShowCheckedModeBanner: false,
      )
    );
  }
}
