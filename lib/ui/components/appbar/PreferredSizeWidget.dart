import 'package:flutter/material.dart';

import '../../../core/configs/BaseColors.dart';

class AppBarWidget {

  static PreferredSizeWidget preferredSizeWidget() {
    return PreferredSize(
      preferredSize: Size.fromHeight(0.0), 
      child: AppBar(
        elevation: 0,
        backgroundColor: BaseColors.primary,
      )
    );
  }
}