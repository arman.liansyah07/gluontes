import 'package:flutter/material.dart';

import '../../../core/configs/BaseColors.dart';
import '../tooltip/TooltipShapeBorder.dart';

class TextFieldWidget extends StatefulWidget {

  final String label;
  final TextEditingController textEditingController;
  final bool labelValid;
  final bool labelValidMessage;
  final ValueChanged<String> onChanged;
  final Function onTapSuffixIcon;
  final IconData prefixIcon; 

  const TextFieldWidget({
    Key? key,
    required this.label,
    required this.textEditingController,
    required this.labelValid,
    required this.labelValidMessage,
    required this.onChanged,
    required this.onTapSuffixIcon,
    required this.prefixIcon
  }) : super(key: key);

  @override
  State<TextFieldWidget> createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 20, 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            alignment: Alignment.topRight,
            children: [
              label(),
              inputText(),
              messageAlert()
            ],
          )
        ],
      ),
    );
  }

  Widget label() {
    return Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
      child: Text("${widget.label}", style: TextStyle(fontSize: 15),),
    );
  }

  Widget inputText() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
      decoration: BoxDecoration(
        color: BaseColors.light,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            blurRadius: 3,
            color: Colors.black.withOpacity(.1),
            offset: Offset(0.0, 0.0),
          ),
        ]
      ),
      height: 45,
      child: TextField(
        style: TextStyle(
          fontSize: 15,
          color: Colors.grey[700]
        ),
        onChanged: widget.onChanged,
        decoration: InputDecoration(
          prefixIcon: Container(
            child: Icon(widget.prefixIcon, color: Colors.grey[400]),
          ),
          suffixIcon: GestureDetector(
            onTap: () {
              widget.onTapSuffixIcon();
            },
            child: Container(
              child: Icon(widget.labelValid ? Icons.check_circle_rounded : Icons.info, color: widget.labelValid ? BaseColors.success : BaseColors.danger,),
            ),
          ),
          hintText: "${widget.label}",
          contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 10),
          border: OutlineInputBorder(),
          hintStyle: TextStyle(
            fontSize: 15
          ),
          enabledBorder: OutlineInputBorder(      
            borderSide: BorderSide(color: widget.labelValid ? BaseColors.light : BaseColors.danger), 
            borderRadius: BorderRadius.circular(5),  
          ),  
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: widget.labelValid ? BaseColors.light : BaseColors.danger),
            borderRadius: BorderRadius.circular(5),
          ),  
        ),
      ),
    );
  }

  Widget messageAlert() {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 700),
      curve: Curves.fastOutSlowIn,
      opacity:  widget.labelValid ? 0 : widget.labelValidMessage? 0 : 1,
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
            child: ClipPath(
              clipper: TriangleClipper(),
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                color: BaseColors.danger,
                height: 10,
                width: 20,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: BaseColors.danger,
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 10, 7),
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Text("${widget.label} cannot be empty", style: TextStyle(fontSize: 12, color: BaseColors.light),),
          ),
        ],
      )
    );
  }
}