import 'package:flutter/material.dart';
import 'package:gluontes/core/viewmodels/BaseProvider.dart';
import 'package:gluontes/ui/components/appbar/PreferredSizeWidget.dart';
import 'package:gluontes/ui/components/textfield/TextFieldWidget.dart';
import 'package:provider/provider.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget.preferredSizeWidget(),
      backgroundColor: Colors.grey[100],
      body: SingleChildScrollView(
        child: Consumer<BaseProvider>(
          builder: (context, provider, _) {
            return Column(
              children: [
                SizedBox(height: 100,),
                TextFieldWidget(
                  label: "Username", 
                  textEditingController: provider.username, 
                  labelValid: provider.usernameValid, 
                  labelValidMessage: provider.usernameValidMessage, 
                  prefixIcon: Icons.person,
                  onChanged: (String value) {
                    if(value == "") {
                      provider.setUsernameValid(false);
                    }
                    else {
                      provider.setUsernameValid(true);
                    }
                  }, 
                  onTapSuffixIcon: () {
                    provider.setUsernameValid(true, isMessage: true);
                  }
                ),
                
                TextFieldWidget(
                  label: "Password", 
                  textEditingController: provider.password, 
                  labelValid: provider.passwordValid, 
                  labelValidMessage: provider.passwordValidMessage, 
                  prefixIcon: Icons.lock,
                  onChanged: (String value) {
                    if(value == "") {
                      provider.setPasswordValid(false);
                    }
                    else {
                      provider.setPasswordValid(true);
                    }
                  }, 
                  onTapSuffixIcon: () {
                    provider.setPasswordValid(true, isMessage: true);
                  }
                ),
              ],
            );
          }
        )
      ),
    );
  }
}